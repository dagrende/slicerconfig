#!/usr/bin/python3
import sys
import os
import configparser
import re
from collections import defaultdict


if len(sys.argv) < 2:
    print("missing argument: missing path to config bundle", file=sys.stderr)
    sys.exit(1)

keyOrderPath = "key-order.tsv"
config = configparser.ConfigParser()
config.read(sys.argv[1])

# build index
subjectsByCategory = defaultdict(list)
keySetByCategory = defaultdict(set)
sectionPattern = re.compile('^(.*):(.*)$')
for section in config.sections():
    sectionMatch = sectionPattern.match(section)
    if sectionMatch:
        category = sectionMatch.group(1)
        subject = sectionMatch.group(2)
        subjectsByCategory[category].append(subject)
        keySetByCategory[category].update(config.options(section))

# read key order file, remove non-exiting keys, add new keys at end of each category, write updated back
orderedKeysByCategory = defaultdict(list)
try:
    with open(keyOrderPath, "r") as fp:
        for line in fp:
            parts = line.strip().split('\t')
            # print(parts)
            if len(parts) != 3:
                print(f'invalid line - should have one tab char: {line}', file=sys.stderr)
                exit(1)
            category = parts[0]
            section = parts[1]
            key = parts[2]
            # print(category, ':', key)
            # exclude key not in keySetByCategory
            if key in keySetByCategory[category]:
                orderedKeysByCategory[category].append((section, key))
    os.rename(keyOrderPath, keyOrderPath + ".old")
except FileNotFoundError:
    print(f'key order file {keyOrderPath} not found - creating it', file=sys.stderr)

#FIXME add keys in keySetByCategory not in orderedKeysByCategory
for category in keySetByCategory:
    for key in keySetByCategory[category]:
        if not next((x for x in orderedKeysByCategory[category] if key == x[1]), None):
            orderedKeysByCategory[category].append(('*new', key))


# write updated keyOrderPath file back
try:
    with open(keyOrderPath, "w") as fp:
        for category in orderedKeysByCategory:
            for section, key in orderedKeysByCategory[category]:
                fp.write(f'{category}\t{section}\t{key}\n')
except:
    print('cound not write file: ' + keyOrderPath, file=sys.stderr)
    exit(1)

# print('orderedKeysByCategory',orderedKeysByCategory)
# print()
# print('subjectsByCategory', subjectsByCategory)
# print()
# print tsv to stdout
for category in subjectsByCategory:
    print(f'{category}\t', end='')
    for subject in subjectsByCategory[category]:
        print(f'\t{subject}', end='')
    print()
    for section, key in orderedKeysByCategory[category]:
        print(f'{category}\t{key}', end='')
        for subject in subjectsByCategory[category]:
            subjectValueByKey = config[f'{category}:{subject}']
            print(f'\t{subjectValueByKey[key]}', end='')
        print()
